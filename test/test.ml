let rec repeat n f =
  if n <= 0 then
    let f () = () in
    f
  else (
    f () ;
    repeat (n - 1) f )

module F379 = Ff.MakeFp (struct
  let prime_order = Z.of_int 379
end)

module Shamir379 = Shamir.Make (F379)

let rec test_simple () =
  let participants = Random.int 100 in
  if 3 >= participants then test_simple ()
  else
    let secret = F379.random () in
    if F379.is_zero secret then test_simple ()
    else
      let shared_points =
        Shamir379.hide_secret ~participants ~threshold:3 secret
      in
      (* List.iter (fun (p1, p2) -> (Printf.printf "s1 = %s, s2 = %s\n") (F379.to_string p1) (F379.to_string p2)) shared_points; *)
      let secret_computed =
        Shamir379.compute_secret
          [ List.nth shared_points 0;
            List.nth shared_points 1;
            List.nth shared_points 2 ]
      in
      (* Printf.printf "Secret = %s. Secret computed = %s\n" (F379.to_string secret) (F379.to_string secret_computed); *)
      assert (secret_computed = secret)

let simple_test () =
  let open Alcotest in
  ("Simple", [test_case "test simple" `Quick (repeat 1000 test_simple)])

let () =
  let open Alcotest in
  run "Test with F379" [simple_test ()]
