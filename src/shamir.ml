module type T = sig
  type scalar

  val hide_secret :
    participants:int -> threshold:int -> scalar -> (scalar * scalar) list

  val compute_secret : (scalar * scalar) list -> scalar
end

module Make (F : Ff.T) = struct
  type scalar = F.t

  module Poly = Polynomial.Make (F)

  let rec generate_random_evaluation_points acc n =
    if n = 0 then acc
    else
      let r = F.random () in
      if List.mem r acc then generate_random_evaluation_points acc n
      else generate_random_evaluation_points (r :: acc) (n - 1)

  let hide_secret ~participants ~threshold secret =
    assert (participants > threshold) ;
    assert (participants > 1) ;
    assert (threshold > 0) ;
    let randoms = List.init (threshold - 1) (fun _i -> F.random ()) in
    let coefficients =
      (secret, threshold - 1)
      :: List.mapi (fun i r -> (r, threshold - i - 2)) randoms
    in
    let polynomial = Poly.of_coefficients coefficients in
    let evaluation_points = generate_random_evaluation_points [] participants in
    (* print_endline (Poly.to_string polynomial); *)
    assert (List.length evaluation_points = participants) ;
    List.map (fun e -> (e, Poly.evaluation polynomial e)) evaluation_points

  let compute_secret coefficients =
    assert (List.length coefficients > 0) ;
    let polynomial = Poly.lagrange_interpolation coefficients in
    (* print_endline (Poly.to_string polynomial); *)
    Poly.get_highest_coefficient polynomial
end
