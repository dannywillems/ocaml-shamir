module type T = sig
  type scalar

  val hide_secret :
    participants:int -> threshold:int -> scalar -> (scalar * scalar) list

  val compute_secret : (scalar * scalar) list -> scalar
end

module Make (F : Ff.T) : T with type scalar = F.t
